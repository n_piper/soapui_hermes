# A Test & Learn for automating Headless SoapUI & JMS Testing

## Goals:  

* SoapUI Pro can be run headlessly via a build server to test Mocked services, JMS (MQ) based services
* Testers / Developers boxes can be provisioned & run same soapUI projects

## What you need on your local machine to run this

* A java JDK, at least 1.6 (Tested at 1.8+)
* Maven (Tested at 3.2+)
* Docker (Not tested yet)
* Vagrant (Tested at 1.8+)

## Remaining Tasks
- [x] Java, Groovy & SoapUI vagrant container
- [x] Determine maven pom settings for smartbear, soapui-pro plugin
- [x] Sample soapUI project that can run headlessly
- [ ] Silent install of SoapUI, Hermes on vagrant container
- [ ] MQ Container starts & runs with 2 queues
- [ ] MQ Consumer consumes from one queue, publishes to another
- [ ] Determine Hermes, IBM MQ maven installs in soapUI POM
- [ ] Modify SoapUI project to start consumer & test queues
- [ ] Determine Hermes, SoapUI install requirements on build server
- [ ] Test Headless build in Bamboo or Jenkins
- [ ] IAG Proxy settings
- [ ] Works on WIndows
- [ ] All doco up to date - a new user can start and run the demo with a maximum of 2 questions

## Install Hermes



### SoapUI pro Container - to run the project locally
* Java JDK
* Groovy
* SoapUI Pro / ReadyUI installed
* Database - like MySQL, Postgres or cheap open source
* SSH in
* Shared data folder /data

### IBM MQ Container with some basic queues for testing purposes

Have used the Docker public image - https://github.com/ibm-messaging/mq-docker

:persevere: 500MB + !!


### Java / IIB container to consume messages of queues - executable container

To be done - proposing

* Sample JMS / Java project - IBM MQ Client - maybe Spring Consumer/Producer example

### SoapUI pro test project - that can run headlessly via maven

https://www.soapui.org/jms/working-with-jms-messages.html
https://www.soapui.org/jms/getting-started.html

**JAR's needed for IBM MQ**

You need the following JAR-files in SoapUI's hermes/lib folder:
* com.ibm.mq.pcf-6.1.jar
* com.ibm.mq.jar
* com.ibm.mqjms.jar
* dhbcore.jar
* connector.jar


### SoapUI Projects - May need multiples - in priority order

* SOAP Webservice / Mock project 
* JMS / MQ / Hermes - Project
* RESTful Webservice / Mock project
* File Transfer / Mock Project
* Database Integration / Mock Project
# A maven repository creator for Hermes IBM MQ Dependencies

## PROBLEM

IBM is not very good at making their developer tools friendly or putting their JAR's into Maven.

To connect via MQ using Hermes is a painful process of discovery as to which chain of Dependent
JAR's are required to connect & browse.

**This is hopefully the final set for version 7.0.1.2**

## CURRENT SOLUTION

The baselined set of tested minimum .jar's are under repository/lib/mqexplorer_7.0.1.2

An installLibraries.bat is created to create a local repository of jar / pom structures for this purpose.

This allows the pom.xml files to reference this created repository and also have it generated.

In future upgrades can be provided by rolling the IBM version without needing a hard reference to .jar files.

_This is likely a temporary fix until registered in a corporate / team repository, the .bat file can likely be modified slightly for that purpose._

## To Run

```
installLibraries.bat
```

A new 'repository' folder is created with the structure of:

groupId:  com.ibm.mqexplorer
artifactId:  (a set of jar files required)
version: 7.0.1.2

## To add the repository JAR's as a dependency

Add a dependency of scope 'system', giving the 'systemPath' attribute for the relative path of the jar file required.
```
<dependency>
			<groupId>com.ibm.mqexplorer</groupId>
			<artifactId>commonservices</artifactId>
			<version>7.0.1.2</version>
			<scope>system</scope>
			<systemPath>${basedir}\..\repository\repository\com\ibm\mqexplorer\commonservices\7.0.1.2\commonservices-7.0.1.2.jar</systemPath>
</dependency>
```
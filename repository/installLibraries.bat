REM # commons-beanutils
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\com.ibm.mq.commonservices_7.0.1.2.jar -DgroupId=com.ibm.mqexplorer -DartifactId=commonservices -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository

REM # commons-codec
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\com.ibm.mq.headers.jar -DgroupId=com.ibm.mqexplorer -DartifactId=headers -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository

REM # commons-collections
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\com.ibm.mq.jar -DgroupId=com.ibm.mqexplorer -DartifactId=mq -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository

REM # commons-httpclient
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\com.ibm.mq.jmqi.jar -DgroupId=com.ibm.mqexplorer -DartifactId=jmqi -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository

REM # commons-io
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\com.ibm.mq.pcf.jar -DgroupId=com.ibm.mqexplorer -DartifactId=pcf -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository

REM # commons-jxpath
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\com.ibm.mq.mqjms.jar -DgroupId=com.ibm.mqexplorer -DartifactId=mqjms -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository

REM # commons-lang
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\connector.jar -DgroupId=com.ibm.mqexplorer -DartifactId=connector -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository

REM # commons-logging
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\dhbcore.jar -DgroupId=com.ibm.mqexplorer -DartifactId=dhbcore -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository

REM # commons-vfs
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\fscontext.jar -DgroupId=com.ibm.mqexplorer -DartifactId=fscontext -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository

REM # dataextendsi
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\jms.jar -DgroupId=com.ibm.mqexplorer -DartifactId=jms -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository

REM #dxsi-loader-utils
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\jta.jar -DgroupId=com.ibm.mqexplorer -DartifactId=jta -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository

REM # Estimage
call mvn install:install-file -Dfile=.\lib\mqexplorer_7.0.1.2\providerutil.jar -DgroupId=com.ibm.mqexplorer -DartifactId=providerutil -Dversion=7.0.1.2 -Dpackaging=jar -DlocalRepositoryPath=.\repository
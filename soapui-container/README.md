# SOAPUI Container

A Centos7 instance installed with Java, Groovy, maven, git, soapUI Pro, Hermes

# Pre-Requisites

## Vagrant with Oracle - Virtual box - tested on version 1.8.1

https://www.vagrantup.com/downloads.html

## Putty / Puttygen on command line (Windows only)

Putty is needed as a Windows SSH Client to ssh into the virtual box - there is a step to convert the vagrant ssh key to a Putty key.

ftp://ftp.chiark.greenend.org.uk/users/sgtatham/putty-latest/x86/putty.exe
ftp://ftp.chiark.greenend.org.uk/users/sgtatham/putty-latest/x86/pageant.exe
ftp://ftp.chiark.greenend.org.uk/users/sgtatham/putty-latest/x86/puttygen.exe



# Running on Windows / IAG SOE

There are 3 known issue on running Vagrant/Oracle Virtual Box on our SOE

* Need to uninstall / re-install virtual box on startup due to issues with Symantec Antivirus on Windows SOE
* Vagrant private key file created needs to be converted to putty format & ssh into box via Putty
* IAG Proxy settings in Vagrant file

## Setting Proxy settings - IAG


## Connecting to SSH 

### Convert the key using Pageant

### Connect using putty & key as vagrant user


## Base install

The instance uses Vagrant as the provisioner.
Centos 7 with Puppet install as base

* Centos7 w/ Puppet - puppetlabs/centos-7.0-64-puppet

# Modules required / installed

* java
* maven
* groovy
* git
* soapUI Pro + License

# To update inside IAG network

>vagrant box add --insecure puppetlabs/centos-7.0-64-puppet

>vagrant up
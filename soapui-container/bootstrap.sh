#!/usr/bin/env bash

# Set timezone to Melbourne, Australia
timedatectl set-timezone Australia/Melbourne 

# Update root certificate - http://kb.kerio.com/product/kerio-connect/server-configuration/ssl-certificates/adding-trusted-root-certificates-to-the-server-1605.html

yum -y reinstall ca-certificates

# Minimal install - support deltarpm's - issue with puppet modules
yum -y install deltarpm

# Update
#yum -y update


echo "RUNNING the Bash"


# install puppet modules
puppet module install puppetlabs-git
puppet module install puppetlabs-java
puppet module install maestrodev-maven
puppet module install rtyler-groovy

puppet apply -d /vagrant_data/puppetInstall.pp 

# SoapUI v5.2.1
wget http://cdn01.downloads.smartbear.com/soapui/5.2.1/SoapUI-5.2.1-linux-bin.tar.gz#_ga=1.214676497.709114441.1454391116

# Unpack , silent install

# hermes Jar 1.14
wget http://downloads.sourceforge.net/project/hermesjms/hermesjms/1.14/hermes-installer-1.14.jar?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fhermesjms%2Ffiles%2Fhermesjms%2F1.14%2F&ts=1455928549&use_mirror=internode

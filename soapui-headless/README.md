# Headless soapui with dependencies

A maven build of a soapui project
pull in dependencies to run a sample test case.

There is a sample soapUI project to run

We are trying to run SoapUI pro plugin at v5.1.1


# Pre-Reqs
* JDK - probably at least 1.6+ (Tested on 1.8)
* mvn 3+

# Advice on how to modify the test for your needs

https://www.soapui.org/jms/working-with-jms-messages.html


# To Run

mvn com.smartbear.soapui:soapui-pro-maven-plugin:5.1.1:test

Currently on a succesful test hitting this issue:  (XPath class cast exception)
http://community.smartbear.com/t5/SoapUI-NG/SU-5-1-2-Maven-plugin-RuntimeException-java-lang/td-p/95234


Maven location

Need to get Hermes & soapUI from the smartbear repository

http://smartbearsoftware.com/repository/maven2/com/smartbear/soapui/
http://smartbearsoftware.com/repository/maven2/hermesjms/hermes/1.14/


IBM Jar's may be a bit harder...


# Adjusting your maven settings.xml to include smartbear repo

A sample file / extract is included here settings_smartbear.xml 

The pluginRepository config should be added to your maven settings.xml

There is a line of XML in soapui-settings.xml - which is located at %USER_HOME%

'''
<con:setting id="ToolsSettings@hermesjms">C:\Program Files\SmartBear\SoapUI-5.2.1\hermesJMS</con:setting>
'''

Could get complex -- 

http://stackoverflow.com/questions/12514808/combining-maven-soapui-and-hermesjms

http://community.smartbear.com/t5/SoapUI-Open-Source/Using-HermesJMS-with-Maven-plugin-and-on-Jenkins/td-p/101648

# What a twisted web we weave....

## This is the dependency hell we'll need to navigate

I have this up and running with the following steps:

* Add the hermes-1.14 dependency in the pom.xml
* Copy a hermesJMS installation to the filesystem of the buildserver
* Add hermes-config.xml to the project, but copy it to the filesystem before every build
* Add soapui-settings.xml to the project and use it with the -tag in the soapui-maven-plugin
* Update soapui-settings.xml with the path of your hermesJMS installation
* Update the SoapUI projectfile with the path of the location of your hermes-config.xml


For the visually inclined among us.. let's look at a local and build machine install:

Used the plantUML editor to get the PNG:
http://www.planttext.com/planttext

src for plantuml diagram is located under src\plantUML

## A potential workaround

http://community.smartbear.com/t5/SoapUI-NG/Export-Preferences/td-p/29588


Today I figured out it is possible to enforce various setting programatically although not possible to distribute as a file.

You can write something like this on the project load script:
```
import com.eviware.soapui.settings.HttpSettings
import com.eviware.soapui.SoapUI 

// setting socket timeout
SoapUI.settings.setString( HttpSettings.SOCKET_TIMEOUT, "90000" )
```


